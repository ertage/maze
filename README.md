# Maze

A Breadth-first search written in C#. 

Winforms was used for the GUI.

# Sample

![Maze](/img/Maze.png)

# Usage

Start the program in your cli with

~~~
maze.exe < maze.dat
~~~

- With arrow keys you can move the Player
- With the "Breitensuche" Button you can start the Breadth-first search,
the player will collect all items automatically

# Make your own Maze

To make your own maze you simply have to create a textfile 
and create it in this format:

~~~
8
6
########
#......#
#..@...#
###....#
#......#
########
~~~

 - The first number represents the column count
 - Second number the Rows
~~~
# = Walls
. = Items
@ = Player
~~~
