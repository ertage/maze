using System;
using System.IO;
using System.Drawing;
using System.Windows.Forms;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

namespace Maze
{
    public class Maze : Form
    {
        ComputerPlayer computerPlayer = new ComputerPlayer();

        private static class Global
        {
            public static Label positionLabel = new Label();
            public static int mazeSpalteInt = 0;
            public static int mazeZeileInt = 0;
            public static char[,] Spielfeld = new char[mazeZeileInt, mazeSpalteInt];
            public static Point spielerPosition = new Point(0, 0);
            public static Boolean hasDot = true;
        }

        protected void CreateButton()
        {
            Button startButton = new Button();
            startButton.Text = "Breitensuche";
            startButton.Size = new Size(100, 25);
            startButton.Font = new Font("Arial", 12);
            startButton.Location = new Point(10, 10);
            startButton.Click += new System.EventHandler(ButtonClick);
            startButton.AutoSize = true;
            startButton.FlatStyle = FlatStyle.Flat;
            Controls.Add(startButton);
        }

        //Breitensuche Aufruf
        protected void ButtonClick(object sender, EventArgs e)
        {
            while (Global.hasDot)
            {
                computerPlayer.Breitensuche();
                Thread.Sleep(100);
                this.Refresh();
            }
        }

        private void CreateLabel()
        {
            Global.positionLabel.Location = new Point(150, 15);
            Global.positionLabel.AutoSize = true;
            Global.positionLabel.Font = new Font("Arial", 12);
            Global.positionLabel.ForeColor = Color.Gray;
            this.Controls.Add(Global.positionLabel);
        }

        private Maze()
        {
            Height = 350;
            Width = 400;
            Text = "Breitensuche - Maze";
            SetStyle(ControlStyles.OptimizedDoubleBuffer, true); //Flackern verhindern
            this.BackColor = ColorTranslator.FromHtml("#282828");
            CreateButton();
            CreateLabel();
        }

        override protected void OnPaint(PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            DrawString(e);
            base.OnPaint(e);
        }

        //Spielfeld zeichnen
        private void DrawString(PaintEventArgs e)
        {
            string drawMaze;
            int x = 15;
            int y = 45;
            StringFormat formatAusrichtung = new StringFormat(StringFormatFlags.NoClip)
            {
                Alignment = StringAlignment.Center
            };
            Font drawFont = new Font("Arial", 16);
            Global.hasDot = false;

            for (int i = 0; i < Global.mazeSpalteInt;)
            {
                for (int j = 0; j < Global.mazeZeileInt;)
                {

                    switch (Global.Spielfeld[j, i])
                    {
                        case '@':
                            drawMaze = Convert.ToString(Global.Spielfeld[j, i]);
                            e.Graphics.DrawString(drawMaze, drawFont, new SolidBrush(ColorTranslator.FromHtml("#d79921")), x, y, formatAusrichtung);
                            Global.spielerPosition.X = i;
                            Global.spielerPosition.Y = j;
                            y = y + 20;
                            j++;
                            break;
                        case '#':
                            drawMaze = Convert.ToString(Global.Spielfeld[j, i]);
                            e.Graphics.DrawString(drawMaze, drawFont, new SolidBrush(ColorTranslator.FromHtml("#928374")), x, y, formatAusrichtung);
                            y = y + 20;
                            j++;
                            break;
                        case '.':
                            drawMaze = Convert.ToString(Global.Spielfeld[j, i]);
                            e.Graphics.DrawString(drawMaze, drawFont, new SolidBrush(ColorTranslator.FromHtml("#fabd2f")),
                            x, y, formatAusrichtung);
                            y = y + 20;
                            j++;
                            Global.hasDot = true;
                            break;
                        default:
                            drawMaze = Convert.ToString(Global.Spielfeld[j, i]);
                            e.Graphics.DrawString(drawMaze, drawFont, new SolidBrush(ColorTranslator.FromHtml("#fabd2f")),
                            x, y, formatAusrichtung);
                            y = y + 20;
                            j++;
                            break;
                    }
                }
                i++;
                x = x + 20;
                y = 45;
            }
            Global.positionLabel.Text = Global.spielerPosition.ToString();
            Global.positionLabel.Refresh();
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            int posY = Global.spielerPosition.Y;
            int posX = Global.spielerPosition.X;

            switch (keyData)
            {
                case (Keys.Down):

                    if (Global.Spielfeld[posY + 1, posX] == '#')
                    {
                    }
                    else
                    {
                        Global.Spielfeld[posY + 1, posX] = '@';
                        Global.Spielfeld[posY, posX] = ' ';
                    }
                    break;
                case (Keys.Up):
                    if (Global.Spielfeld[posY - 1, posX] == '#')
                    {
                    }
                    else
                    {
                        Global.Spielfeld[posY - 1, posX] = '@';
                        Global.Spielfeld[posY, posX] = ' ';
                    }
                    break;
                case (Keys.Left):
                    if (Global.Spielfeld[posY, posX - 1] == '#')
                    {
                    }
                    else
                    {
                        Global.Spielfeld[posY, posX - 1] = '@';
                        Global.Spielfeld[posY, posX] = ' ';
                    }
                    break;
                case (Keys.Right):
                    if (Global.Spielfeld[posY, posX + 1] == '#')
                    {
                    }
                    else
                    {
                        Global.Spielfeld[posY, posX + 1] = '@';
                        Global.Spielfeld[posY, posX] = ' ';
                    }
                    break;
                default:
                    break;
            }
            this.Refresh();
            return base.ProcessCmdKey(ref msg, keyData);
        }

        public class ComputerPlayer
        {
            private Point MoveDirection(Point aktuellePosition, string richtung)
            {
                Point moveDirection = new Point(0, 0);
                switch (richtung)
                {
                    case "Hoch":
                        moveDirection.Y = aktuellePosition.Y - 1;
                        moveDirection.X = aktuellePosition.X;
                        break;
                    case "Runter":
                        moveDirection.Y = aktuellePosition.Y + 1;
                        moveDirection.X = aktuellePosition.X;
                        break;
                    case "Links":
                        moveDirection.Y = aktuellePosition.Y;
                        moveDirection.X = aktuellePosition.X - 1;
                        break;
                    case "Rechts":
                        moveDirection.Y = aktuellePosition.Y;
                        moveDirection.X = aktuellePosition.X + 1;
                        break;
                }
                return moveDirection;
            }

            //Phase1
            public void Breitensuche()
            {
                Point aktuellePosition = new Point(0, 0);
                Queue<Point> queuePosition = new Queue<Point>();
                Hashtable mazeHash = new Hashtable();
                aktuellePosition = Global.spielerPosition;
                queuePosition.Enqueue(aktuellePosition);

                aktuellePosition = queuePosition.Dequeue();
                Point neuePosition = new Point(0, 0);
                string[] Richtungen = { "Links", "Hoch", "Rechts", "Runter" };

                foreach (string richtung in Richtungen)
                {
                    neuePosition = MoveDirection(aktuellePosition, richtung);
                    if (Global.Spielfeld[neuePosition.Y, neuePosition.X] == ' '
                    && mazeHash.ContainsKey(neuePosition) == false)
                    {
                        queuePosition.Enqueue(neuePosition);
                        mazeHash.Add(neuePosition, aktuellePosition.GetHashCode());
                    }
                    else if (Global.Spielfeld[neuePosition.Y, neuePosition.X] == '.')
                    {
                        queuePosition.Clear();
                        break;
                    }

                }
                while (queuePosition.Count != 0)
                {
                    aktuellePosition = queuePosition.Dequeue();

                    foreach (string richtung in Richtungen)
                    {
                        neuePosition = MoveDirection(aktuellePosition, richtung);

                        if (Global.Spielfeld[neuePosition.Y, neuePosition.X] == ' '
                        && mazeHash.ContainsKey(neuePosition) == false)
                        {
                            queuePosition.Enqueue(neuePosition);
                            mazeHash.Add(neuePosition, aktuellePosition.GetHashCode());
                        }
                        else if (Global.Spielfeld[neuePosition.Y, neuePosition.X] == '.')
                        {
                            queuePosition.Clear();
                            break;
                        }
                    
                    }
                }
                ItemSammeln(mazeHash, neuePosition, aktuellePosition);
            }

            //Phase2
            private void ItemSammeln(Hashtable mazeHash, Point neuePosition, Point aktuellePosition)
            {
                Stack wegZumItem = new Stack();
                wegZumItem.Push(neuePosition);
                Point tempf = new Point(0, 0);

                if (Global.Spielfeld[aktuellePosition.Y, aktuellePosition.X] == '@')
                {
                    Global.Spielfeld[neuePosition.Y, neuePosition.X] = '@';
                    Global.Spielfeld[Global.spielerPosition.Y, Global.spielerPosition.X] = ' ';
                    Global.spielerPosition = neuePosition;
                    wegZumItem.Clear();
                }
                else
                {
                    while (Global.Spielfeld[neuePosition.Y, neuePosition.X] != '@')
                    {
                        neuePosition = ItemVergleich(mazeHash, aktuellePosition);
                        aktuellePosition = neuePosition;
                        wegZumItem.Push(neuePosition);

                    }
                    tempf = neuePosition;
                    while (wegZumItem.Count != 0)
                    {
                        Global.Spielfeld[tempf.Y, tempf.X] = ' ';
                        Point temp = (Point)wegZumItem.Pop();
                        Global.Spielfeld[temp.Y, temp.X] = '@';
                        tempf = temp;
                        neuePosition = temp;
                    }
                    Global.spielerPosition = neuePosition;
                }
            }

            private Point ItemVergleich(Hashtable mazeHash, Point aktuellePosition)
            {
                int value = (int)mazeHash[aktuellePosition];
                int spielerhash = Global.spielerPosition.GetHashCode();
                int tempNeuPos;
                Point gefunden = new Point(0, 0);
                foreach (DictionaryEntry item in mazeHash)
                {
                    Point tempKey = (Point)item.Key;
                    tempNeuPos = tempKey.GetHashCode();
                    int tempValue = (int)item.Value;
                    gefunden = (Point)item.Key;
                    Boolean areEqual = tempValue.Equals(spielerhash);
                    if (areEqual == false)
                    {
                        Boolean equal = tempNeuPos.Equals(value);
                        if (equal == true)
                        {
                            break;
                        }
                    }
                    else if (areEqual == true)
                    {
                        gefunden = Global.spielerPosition;
                        break;
                    }
                }
                return gefunden;
            }

            public static void Main(string[] args)
            {
                //Einlesen der maze.dat und Speichern als Spielfeld.
                StreamReader streamReader = new StreamReader(args[0]);
                string mazeSpalte = streamReader.ReadLine();
                string mazeZeile = streamReader.ReadLine();
                Global.mazeSpalteInt = Int32.Parse(mazeSpalte);
                Global.mazeZeileInt = Int32.Parse(mazeZeile);
                char[] oneDimensionalMaze = new char[Global.mazeZeileInt * Global.mazeSpalteInt];
                string mazeSpielFeld = streamReader.ReadToEnd();
                string cleanedMazeSpielFeld = mazeSpielFeld.Replace("\n", "").Replace(" ", "");
                oneDimensionalMaze = cleanedMazeSpielFeld.ToCharArray(0, Global.mazeZeileInt * Global.mazeSpalteInt);
                Global.Spielfeld = new char[Global.mazeZeileInt, Global.mazeSpalteInt];
                int index = 0;
                for (int x = 0; x < Global.mazeZeileInt; x++)
                {
                    for (int y = 0; y < Global.mazeSpalteInt; y++)
                    {
                        Global.Spielfeld[x, y] = oneDimensionalMaze[index];
                        index++;
                    }
                }

                streamReader.Close();
                Application.Run(new Maze());
            }
        }
    }
}
